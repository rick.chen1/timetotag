#!/usr/bin/env bash
# fail if any commands fails
set -e
# make pipelines' return status equal the last command to exit with a non-zero status, or zero if all commands exit successfully
set -o pipefail
# debug log
set -x

GOOGLE_SHEET_PAGE="iOS"
APP_DEVELOP_BRANCH="develop"

TIMETOTAG_DEST_DIR="$BITRISE_SOURCE_DIR/_timetotag"
TIMETOTAG_GIT_URL="gitlab.com:rick.chen1/timetotag.git"

APPSRC_DEST_DIR="$BITRISE_SOURCE_DIR/"

git clone "git@$TIMETOTAG_GIT_URL" "$TIMETOTAG_DEST_DIR"

CSV_GENERATION_SCRIPT="$TIMETOTAG_DEST_DIR/git-commit-to-release-time.sh"

LEAD_TIME_CSV_NAME="lead_time.csv"

pushd "$APPSRC_DEST_DIR" || exit 1
git fetch origin master $APP_DEVELOP_BRANCH
sh -c "$CSV_GENERATION_SCRIPT" > "$LEAD_TIME_CSV_NAME"
popd || exit 2

LEAD_TIME_CSV_PATH=$(realpath "$BITRISE_SOURCE_DIR/$LEAD_TIME_CSV_NAME")

pushd "$TIMETOTAG_DEST_DIR" || exit 1
swift run TimeToTag -c 5 "$LEAD_TIME_CSV_PATH" --page-name "$GOOGLE_SHEET_PAGE" --upload-to-sheets
popd || exit 2
