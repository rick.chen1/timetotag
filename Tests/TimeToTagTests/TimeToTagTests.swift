import XCTest
@testable import TimeToTag

enum VersionLeadTime: Equatable {
    case version(String, leadTime: Double)
}

final class TimeToTagTests: XCTestCase {
    func testDaysDiff() throws {

        let csvPath = Bundle.module.path(forResource: "days_diff", ofType: "csv")

        XCTAssertNotNil(csvPath)

        if let csvPath = csvPath {
            let csv = try loadCSV(csvPath)
            let dd = try csv.toDaysDiff().sorted()
            let versionLeadTimes = dd.map {
                VersionLeadTime.version($0.version, leadTime: $0.leadTime)
            }
            XCTAssertEqual(versionLeadTimes, leadTimeResults)
        } else {
            XCTFail()
        }
    }

    func testEncoding() throws {

        guard let date = "2022-11-21T10:04:22+13:00".asDate else {
            XCTFail()
            return
        }

        let daysDiff = [
            DaysDiff(version: "999.0", tagDate: date, daysToTag: [10, 11, 12, 13], commitCount: 53, jiras: ["XX-1", "YYY-42"]),
            DaysDiff(version: "123.1", tagDate: date, daysToTag: [20, 9, 11, 5], commitCount: 87, jiras: ["AB-3", "CC-99"]),
        ]

        let leadTime = daysDiff.map(LeadTimeData.fromDaysDiff(_:))

        let json = try LeadTimeData.encodeToJson(leadTime)

        let decoded = try LeadTimeData.decodeFromJson(json.data(using: .utf8)!)

        XCTAssertEqual(leadTime, decoded)
    }
}

let leadTimeResults: [VersionLeadTime] = [
    .version("73.0", leadTime: 9.5),
    .version("74.0", leadTime: 12.0),
    .version("75.0", leadTime: 26.0),
    .version("76.0", leadTime: 25.0),
    .version("77.0", leadTime: 19.0),
    .version("78.0", leadTime: 25.0),
    .version("79.0", leadTime: 12.0),
    .version("80.0", leadTime: 11.0),
    .version("81.0", leadTime: 33.0),
    .version("82.0", leadTime: 14.0),
    .version("83.0", leadTime: 12.0),
    .version("84.0", leadTime: 22.0),
    .version("85.0", leadTime: 33.0),
    .version("86.0", leadTime: 24.0),
    .version("87.0", leadTime: 13.0),
    .version("88.0", leadTime: 17.0),
    .version("89.0", leadTime: 18.0),
    .version("89.1", leadTime: 0.0),
    .version("90.0", leadTime: 14.0),
    .version("91.0", leadTime: 13.0),
    .version("91.1", leadTime: 0.0),
    .version("91.3", leadTime: 0.0),
    .version("92.0", leadTime: 12.0),
    .version("92.1", leadTime: 0.0),
    .version("92.2", leadTime: 2.0),
    .version("93.0", leadTime: 19.0),
    .version("93.1", leadTime: 2.0),
    .version("93.2", leadTime: 0.0),
    .version("93.3", leadTime: 0.0),
    .version("94.0", leadTime: 25.0),
    .version("94.1", leadTime: 0.0),
    .version("94.2", leadTime: 0.0),
    .version("95.0", leadTime: 22.0),
    .version("95.1", leadTime: 5.0),
    .version("96.0", leadTime: 10.0),
    .version("96.1", leadTime: 5.0),
    .version("96.2", leadTime: 0.0),
    .version("97.0", leadTime: 21.0),
    .version("97.1", leadTime: 0.0),
    .version("98.0", leadTime: 12.0),
    .version("99.0", leadTime: 9.0),
    .version("99.1", leadTime: 1.0),
    .version("100.0", leadTime: 34.0),
    .version("100.1", leadTime: 0.0),
    .version("101.0", leadTime: 20.0),
    .version("102.0", leadTime: 16.5),
    .version("102.1", leadTime: 1.0),
    .version("103.0", leadTime: 17.0),
    .version("104.0", leadTime: 17.0),
    .version("105.0", leadTime: 17.0),
    .version("105.1", leadTime: 1.0),
    .version("106.0", leadTime: 17.0),
    .version("106.1", leadTime: 1.0),
    .version("107.0", leadTime: 18.0),
    .version("108.0", leadTime: 17.0),
    .version("109.0", leadTime: 16.0),
    .version("110.0", leadTime: 13.0),
    .version("110.1", leadTime: 2.0),
    .version("111.0", leadTime: 12.0),
    .version("112.0", leadTime: 23.0),
    .version("113.0", leadTime: 27.0),
    .version("113.1", leadTime: 1.0),
    .version("115.0", leadTime: 23.0),
    .version("115.1", leadTime: 4.0),
    .version("116.0", leadTime: 13.0),
    .version("116.1", leadTime: 2.0),
    .version("117.0", leadTime: 24.5),
    .version("118.0", leadTime: 19.0),
    .version("119.0", leadTime: 11.0),
    .version("120.0", leadTime: 16.0),
    .version("120.1", leadTime: 1.0),
    .version("121.0", leadTime: 12.0),
    .version("122.0", leadTime: 13.0)
]
