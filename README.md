# TimeToTag

A CLI tool to convert CSV files generated from `git-commit-to-release-time.sh` into JSON and/or post results to a Google Sheet.

```
USAGE: time-to-tag [<csv-path>] [--page-name <page-name>] [--count <count>] [--include-hotfixes] [--upload-to-sheets] [--dump-json]

ARGUMENTS:
  <csv-path>              Path to CSV file.

OPTIONS:
  --page-name <page-name> Name of page on Google Sheet - iOS or Android
  -c, --count <count>     The number of releases to show. (default: 5)
  --include-hotfixes      Include hot fixes.
  --upload-to-sheets      Push results to google sheets.
  --dump-json             Dump Json to stdout.
  -h, --help              Show help information.
```
