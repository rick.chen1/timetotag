import ArgumentParser

import SwiftCSV

import Foundation

@main
struct TimeToTag: AsyncParsableCommand {
    @Argument(help: "Path to CSV file.")
    var csvPath: String = ""

    @Option(help: "Name of page on Google Sheet - iOS or Android")
    var pageName: String = ""

    @Option(name: .shortAndLong, help: "The number of releases to show.")
    var count: Int = 5

    @Flag(help: "Include hot fixes.")
    var includeHotfixes = false

    @Flag(help: "Push results to google sheets.")
    var uploadToSheets = false

    @Flag(help: "Dump Json to stdout.")
    var dumpJson = false

    mutating func run() async throws {
        guard FileManager.default.fileExists(atPath: csvPath) else {
            throw TTTError.csvNotFound(csvPath)
        }

        if uploadToSheets && pageName.isEmpty {
            throw TTTError.sheetPageNameNotProvided
        }

        let leadTimes = try loadCSV(csvPath)
            .toDaysDiff()
            .sorted()
            .reversed()
            .filter {
                includeHotfixes || $0.version.hasSuffix(".0")
            }

        let filteredLeadTimes = leadTimes
            .prefix(count)
            .map(LeadTimeData.fromDaysDiff(_:))

        if dumpJson {
            let json = try LeadTimeData.encodeToJson(filteredLeadTimes)
            print(json)
        }

        if uploadToSheets {
            let sheets = GoogleSheets()
            let releases = try await sheets.getColumn("\(pageName)!A2:A")

            let rowsToAppend = filteredLeadTimes
                .sorted()
                .filter { leadTime in
                    releases.contains(leadTime.version) == false
                }

            if releases.count != rowsToAppend.count {
                let releaseSet = Set(releases)
                let alreadyThereSet = releaseSet.intersection(filteredLeadTimes.map(\.version))
                print("Some releases are already on the sheet. Not appending those - \(alreadyThereSet.sorted().joined(separator: ", "))")
            }

            if !rowsToAppend.isEmpty {
                print("Appending rows for versions: \(rowsToAppend.map(\.version).joined(separator: ", "))")
                try await sheets.appendRows(
                    rowsToAppend.map { $0.toRow() },
                    page: pageName
                )
            }
        }
    }
}

enum TTTError: Error {
    case csvNotFound(String)
    case sheetPageNameNotProvided
    case serviceAccountJsonUnavailable
    case serviceAccountKeyJsonUnavailable
}
