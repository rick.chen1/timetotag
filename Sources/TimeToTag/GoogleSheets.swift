import Foundation

private let spreadsheetId = "1IDxqeqHISyRLO6qSL9gFppEOZLwdwre0rimOUAcRByM"
private let apiKey = "AIzaSyDz0BsOXUv-lIW8nrCMvLfmHkygwXaxh0c"

class GoogleSheets {


    func getColumn(_ page: String) async throws -> [String] {
        guard let url = URL(string: "https://sheets.googleapis.com/v4/spreadsheets/\(spreadsheetId)/values/\(page)?majorDimension=COLUMNS&key=\(apiKey)") else {
            throw GoogleSheetsError.invalidUrl
        }

        let (data, _) = try await URLSession.shared.data(from: url)

        let decoder = JSONDecoder()
        let valueRange = try decoder.decode(ValueRange.self, from: data)
        return valueRange.values.flatMap { $0 }
    }


    func appendRows(_ rows: [[String]], page: String) async throws {
        guard let url = URL(string: "https://sheets.googleapis.com/v4/spreadsheets/\(spreadsheetId)/values/\(page):append?valueInputOption=USER_ENTERED") else {
            throw GoogleSheetsError.invalidUrl
        }

        let body = ValueRange(range: page, majorDimension: "ROWS", values: rows)
        let bodyData = try body.encodeToJson()
        let jwtToken = try jwt()

        let accessToken = try await GCPJWT.tokenExchange(jwtToken: jwtToken)

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "content-type")
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")

        let (_, _) = try await URLSession.shared.data(for: request)
    }


    private func jwt() throws -> String {
        guard let serviceAccountKeyJsonUrl = Bundle.module.url(forResource: "starstosheets-1535327499823-b742d1ed53dd",
                                                               withExtension: "json",
                                                               subdirectory: "Resources") else { throw TTTError.serviceAccountKeyJsonUnavailable }




        let signedJwt = try GCPJWT().jwt(keyJsonUrl: serviceAccountKeyJsonUrl)
        return signedJwt
    }

}

private enum GoogleSheetsError: Error {
    case invalidUrl
}

private enum Dimension: Codable {
    case ROWS
    case COLUMNS
}

private struct ValueRange: Codable {
    let range: String
    let majorDimension: String
    let values: [[String]]

    func encodeToJson() throws -> Data {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(displayFormatter)
        let jsonData = try encoder.encode(self)
        return jsonData
    }
}
