import SwiftCSV

import Foundation

typealias Version = String

struct LeadTimeData: Codable, Equatable, Comparable, Hashable {
    let version: Version
    let tagDate: Date
    let leadTime: Double
    let commitCount: Int
    let jiras: [String]

    public static func < (lhs: Self, rhs: Self) -> Bool {
        if let lv = Double(lhs.version), let rv = Double(rhs.version) {
            return lv < rv
        }
        return lhs.version < rhs.version
    }
}

extension LeadTimeData {
    static func fromDaysDiff(_ daysDiff: DaysDiff) -> Self {
        LeadTimeData(
            version: daysDiff.version,
            tagDate: daysDiff.tagDate,
            leadTime: daysDiff.leadTime,
            commitCount: daysDiff.commitCount,
            jiras: daysDiff.jiras
        )
    }

    static func encodeToJson(_ data: [LeadTimeData]) throws -> String {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(jsonDateFormatter)
        encoder.outputFormatting = .prettyPrinted
        let jsonData = try encoder.encode(data)
        return String(data: jsonData, encoding: .utf8)!
    }

    static func decodeFromJson(_ data: Data) throws -> [LeadTimeData] {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(jsonDateFormatter)
        let leadTimeData = try decoder.decode([LeadTimeData].self, from: data)
        return leadTimeData
    }

    func toRow() -> [String] {
        [version, nzDateFormatter.string(from: tagDate), "\(leadTime)", "\(commitCount)", "\(jiras.joined(separator: ", "))"]
    }
}

struct DaysDiff: Comparable {
    let version: Version
    let tagDate: Date
    let daysToTag: [Int]
    let commitCount: Int
    let jiras: [String]

    var leadTime: Double {
        daysToTag.median()
    }

    public static func < (lhs: Self, rhs: Self) -> Bool {
        if let lv = Double(lhs.version), let rv = Double(rhs.version) {
            return lv < rv
        }
        return lhs.version < rhs.version
    }
}

func loadCSV(_ path: String) throws -> NamedCSV {
    let csvFile: CSV = try NamedCSV(url: URL(fileURLWithPath:path))
    return csvFile
}

extension CSV where DataView == Named {

    func toDaysDiff() throws -> [DaysDiff] {
        var dict: [Version: DaysDiff] = [:]

        try self.enumerateAsDict { row in
            if let version = row["released_version"],
               let daysDiff = row["days_diff"],
               let daysDiffInt = Int(daysDiff),
               let tagDateString = row["earliest_tag_date"],
               let tagDate = tagDateString.asDate {

                let daysToTag = (dict[version]?.daysToTag ?? []) + [daysDiffInt]
                let commitCount = (dict[version]?.commitCount ?? 0) + 1
                let jiras = Set(((dict[version]?.jiras ?? []) + [row["jira"]].compactMap { $0 }))
                dict[version] = DaysDiff(
                    version: version,
                    tagDate: tagDate,
                    daysToTag: daysToTag,
                    commitCount: commitCount,
                    jiras: Array(jiras).filter { !$0.isEmpty }.sorted()
                )
            }
        }

        return dict.map { _, value in
            value
        }
    }
}

let dateFormatter = ISO8601DateFormatter()

let displayFormatter = createDateFormatter()

func createDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm'"
    return dateFormatter
}

let jsonDateFormatter = createJSONDateFormatter()

func createJSONDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZ'"
    return dateFormatter
}

let nzDateFormatter = createNZDateFormatter()

func createNZDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss'"
    return dateFormatter
}

extension String {

    var asDate: Date? {
        dateFormatter.date(from: self)
    }
}


extension Array where Element == Int {
    func median() -> Double {
        let sortedArray = sorted()
        if count % 2 != 0 {
            return Double(sortedArray[count / 2])
        } else {
            return Double(sortedArray[count / 2] + sortedArray[count / 2 - 1]) / 2.0
        }
    }
}

