import Foundation
import SwiftJWT

struct GCPJWT {

	func jwt(keyJsonUrl: URL) throws -> String {
        let key = try loadPrivateKey(keyJsonUrl)
        let header = Header(typ: "JWT", kid: key.privateKeyId)
        let claims = MyClaims(
            iss: key.clientEmail,
            sub: key.clientEmail,
            iat: Date(),
            exp: Date(timeIntervalSinceNow: 3600),
            aud: "https://oauth2.googleapis.com/token",
            scope: "https://www.googleapis.com/auth/spreadsheets")

        guard let privateKey = key.privateKey.data(using: .utf8) else {
            throw GCPJWTError.badPrivateKey
        }

        let jwtSigner = JWTSigner.rs256(privateKey: privateKey)
        var jwt = JWT(header: header, claims: claims)
        let signedJWT = try jwt.sign(using: jwtSigner)
        return signedJWT
	}

	func loadServiceAccountDeets(
	    _ serviceAccountJsonUrl: URL) throws -> GCPClientSecret
	{
    	let data = try Data(contentsOf: serviceAccountJsonUrl)
    	let decoder = JSONDecoder()
    	return try decoder.decode(GCPClientSecret.self, from: data)
	}

	func loadPrivateKey(_ serviceAccountKeyJsonUrl: URL) throws -> GCPKey {
    	let data = try Data(contentsOf: serviceAccountKeyJsonUrl)
    	let decoder = JSONDecoder()
    	return try decoder.decode(GCPKey.self, from: data)
	}

    static func tokenExchange(jwtToken: String) async throws -> String {
        guard let url = URL(string: "https://oauth2.googleapis.com/token") else {
            throw GCPJWTError.badTokenExchangeUrl
        }

        let body = "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=\(jwtToken)"

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = body.data(using: .utf8)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")

        let (data, response) = try await URLSession.shared.data(for: request)

        if let response = response as? HTTPURLResponse, response.statusCode != 200 {
            let value = String(data: data, encoding: .utf8)!
            throw GCPJWTError.badTokenExchange(value)
        }

        let decoder = JSONDecoder()
        let tokenSwap = try decoder.decode(GCPTokenSwap.self, from: data)
        return tokenSwap.accessToken
    }
}

struct GCPTokenSwap: Codable {
    let accessToken: String
    let expiresIn: Int
    let tokenType: String

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        accessToken = try values.decode(String.self, forKey: .accessToken)
        expiresIn = try values.decode(Int.self, forKey: .expiresIn)
        tokenType = try values.decode(String.self, forKey: .tokenType)
   }

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case tokenType = "token_type"
    }
}

struct MyClaims: Claims {
    let iss: String
    let sub: String
    let iat: Date
    let exp: Date
    let aud: String
    let scope: String
}

struct GCPKey: Codable {
    let type: String
    let projectId: String
    let privateKeyId: String
    let privateKey: String
    let clientEmail: String
    let clientId: String
    let authUri: URL
    let tokenUri: URL?
    let authProviderX509CertUrl: URL?
    let clientX509CertUrl: URL?

 	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decode(String.self, forKey: .type)
        projectId = try values.decode(String.self, forKey: .projectId)
        privateKeyId = try values.decode(String.self, forKey: .privateKeyId)
        privateKey = try values.decode(String.self, forKey: .privateKey)
        clientEmail = try values.decode(String.self, forKey: .clientEmail)
        clientId = try values.decode(String.self, forKey: .clientId)
		authUri = try values.decode(URL.self, forKey: .authUri)
		tokenUri = try values.decode(URL.self, forKey: .tokenUri)
		authProviderX509CertUrl = try values.decode(URL.self, forKey: .authProviderX509CertUrl)
        clientX509CertUrl = try values.decode(URL.self, forKey: .clientX509CertUrl)
	}

 	enum CodingKeys: String, CodingKey {
        case type = "type"
		case projectId = "project_id"
        case privateKeyId = "private_key_id"
        case privateKey = "private_key"
        case clientEmail = "client_email"
        case clientId = "client_id"
		case authUri = "auth_uri"
		case tokenUri = "token_uri"
		case authProviderX509CertUrl = "auth_provider_x509_cert_url"
		case clientX509CertUrl = "client_x509_cert_url"
	}
}

struct GCPClientSecret: Codable {
    let installed: GCPServiceAccount
}

struct GCPServiceAccount: Codable {
    let clientId: String
    let projectId: String
    let authUri: URL
    let tokenUri: URL
    let authProviderX509CertUrl: URL
    let clientSecret: String
    let redirectUris: [URL]

 	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		clientId = try values.decode(String.self, forKey: .clientId)
		projectId = try values.decode(String.self, forKey: .projectId)
		authUri = try values.decode(URL.self, forKey: .authUri)
		tokenUri = try values.decode(URL.self, forKey: .tokenUri)
		authProviderX509CertUrl = try values.decode(URL.self, forKey: .authProviderX509CertUrl)
		clientSecret = try values.decode(String.self, forKey: .clientSecret)
		redirectUris = try values.decode([URL].self, forKey: .redirectUris)
	}            

 	enum CodingKeys: String, CodingKey {
		case clientId = "client_id"
		case projectId = "project_id"
		case authUri = "auth_uri"
		case tokenUri = "token_uri"
		case authProviderX509CertUrl = "auth_provider_x509_cert_url"
		case clientSecret = "client_secret"
		case redirectUris = "redirect_uris"
	}      
}

enum GCPJWTError: Error {
    case badPrivateKey
    case badTokenExchangeUrl
    case badTokenExchange(String)
}
