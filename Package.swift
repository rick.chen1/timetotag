// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TimeToTag",
    platforms: [
        .macOS(.v12),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://github.com/swiftcsv/SwiftCSV.git", from: "0.8.0"),
        .package(url: "https://github.com/apple/swift-argument-parser", from: "1.2.0"),
        .package(url: "https://github.com/Kitura/Swift-JWT.git", .upToNextMajor(from: "3.6.2")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .executableTarget(
            name: "TimeToTag",
            dependencies: [
                .product(name: "SwiftCSV", package: "SwiftCSV"),
                .product(name: "ArgumentParser", package: "swift-argument-parser"),
                .product(name: "SwiftJWT", package: "Swift-JWT"),
            ],
            resources: [
                .copy("Resources")
            ]),
        .testTarget(
            name: "TimeToTagTests",
            dependencies: ["TimeToTag"],
            resources: [
                .copy("Resources")
            ]),
    ]
)
