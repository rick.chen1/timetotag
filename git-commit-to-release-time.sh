#!/bin/bash

# set -x

monolib_coco_commit="f7448e63bc888bc8088ecaff04e238c6b647b2aa"
android_coco_commit="550f6073aaf7438118a7f1d1b939cd78106b1503"
android_coco_tangram_commit="19160a283bd55bca9c34654d3398577955fe0e96"

beginswith() { case $2 in "$1"*) true;; *) false;; esac; }

if [ "$(uname)" == "Darwin" ]; then
  date_bin="gdate" # brew install coreutils
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  date_bin="date"
else
  # not supported
  exit 88
fi

$(git cat-file -e $android_coco_commit)
if [ $? -eq "0" ]; then
  is_android=1
  target_branch="origin/development"
else
  $(git cat-file -e $monolib_coco_commit)
  if [ $? -eq "0" ]; then
    is_ios=1
    target_branch="origin/develop"
  else
    echo "Not Android or iOS. Fail"
    exit 33
  fi
fi

commits=$(git rev-list origin/master --no-merges --since=3.months.ago)

# https://stackoverflow.com/questions/8475448/find-merge-commit-which-include-a-specific-commit
function git-find-merge {
    local commit=$1
    local branch=${2:-HEAD}
    local output=$((git rev-list $commit..$branch --ancestry-path | /bin/cat -n; git rev-list $commit..$branch --first-parent | /bin/cat -n) | sort -k2 -s | uniq -f1 -d | sort -n | tail -1 | cut -f2)
    echo "$output"
}

echo "commit,commit_date,earliest_tag_date,released_version,days_diff,jira"
for ix in ${commits[@]}; do

  when_merged=$(git-find-merge $ix $target_branch)
  if [[ $is_android == 1 ]] && [[ "$when_merged" == "$android_coco_commit" || "$when_merged" == "$android_coco_tangram_commit" ]]; then
    continue
  fi

  if [[ $is_ios == 1 ]] && [[ "$when_merged" == "$monolib_coco_commit" ]]; then
    continue
  fi

  author_email=$(git show -s --format=%ae "$ix")
  if [[ "$author_email" == "please-set-your-email@bitrise.io" ]] || beginswith "mobile@" "$author_email"; then
    continue
  fi
  commit_date=$(git show -s --format="%cI" "$ix")
  commit_subject=$(git show -s --format="%s" "$ix")
  jiraCaseRegex="([A-Z]+-[0-9]+)"
  if [[ $commit_subject =~ $jiraCaseRegex ]]
  then
    jiraCaseId="${BASH_REMATCH[1]}"
  else
    jiraCaseId=""
  fi
  earliest_tag_date_comma_tag=$(git tag --list "v*" --contains "$ix" --sort=version:refname --format="%(creatordate:iso-strict),%(refname:short)" | grep -v '\.1$' | head -n 1)
  earliest_tag_date=${earliest_tag_date_comma_tag%,*}
  earliest_version=${earliest_tag_date_comma_tag#*,v}
  days_diff=$(( ($($date_bin +%s -d "$earliest_tag_date")-$($date_bin +%s -d "$commit_date")) / 86400 ))
  echo "$ix,$commit_date,$earliest_tag_date,$earliest_version,$days_diff,$jiraCaseId"
done
